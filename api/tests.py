from django.test import SimpleTestCase
from django.urls import reverse, resolve
from api.views import ViewAllPersons, PersonView, UpdatePerson
from rest_framework.test import APITestCase, APIClient
from rest_framework import status

# Create your tests here.
class ApiUrlsTests(SimpleTestCase):

    def test_get_persons_is_resolved(self):
        url = reverse('all-persons')
        self.assertEqual(resolve(url).func.view_class, ViewAllPersons)

    def test_get_person_is_resolved(self):
        url = reverse('view-person', args=[1])
        self.assertEqual(resolve(url).func.view_class, PersonView)

class ViewAllPersonsAPIViewTests(APITestCase):

    persons_url = reverse('all-persons')

    def test_post_person(self):
        data = {
            "name": "hakuna matata",
            "email": "hakuna@matata.com",
            "phone": "12345"
        }
        response = self.client.post(self.persons_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'], 'hakuna matata')
        self.assertEqual(response.data['email'], 'hakuna@matata.com')
        self.assertEqual(response.data['phone'], '12345')

    def test_get_all_persons(self):
        response = self.client.get(self.persons_url)
        self.assertEqual(response.status_code, 200)
class PersonViewAPIViewTests(APITestCase):

    person_url = reverse('view-person', args=[1])
    persons_url = reverse('all-persons')

    def test_get_person(self):
        data = {
            "name": "hakuna matata",
            "email": "hakuna@matata.com",
            "phone": "12345"
        }
        self.client.post(self.persons_url, data, format='json')
        response = self.client.get(self.person_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], "hakuna matata")

class UpdatePersonAPIViewTests(APITestCase):
    persons_url = reverse('all-persons')
    update_url = reverse('update-person', args=[1])

    def setUp(self):
        data = {
            "name": "hakuna matata",
            "email": "hakuna@matata.com",
            "phone": "12345"
        }
        self.client.post(self.persons_url, data, format='json')

    def test_update_person(self):
        new_data = {
            "name": "cinnamon",
            "email": "cinnamon@buns.com",
            "phone": "433443"
        }
        response = self.client.put(self.update_url, new_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_customer(self):
        response = self.client.delete(self.update_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

