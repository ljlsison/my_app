from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from api.serializers import PersonSerializer
from crud.models import Person
from rest_framework import status
from functools import wraps

# Create your views here.
class ViewAllPersons(APIView):

    def get(self, request, format=None):
        persons = Person.objects.all()
        serializer = PersonSerializer(persons, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PersonSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def resource_checker(model):
    def check_entiy(fun):
        @wraps(fun)
        def inner_function(*args, **kwargs):
            try:
                x = fun(*args, **kwargs)
                return x
            except model.DoesNotExist:
                return Response({'msg': 'Not Found'}, status=status.HTTP_204_NO_CONTENT)
        return inner_function
    return check_entiy

class PersonView(APIView):

    @resource_checker(Person)
    def get(self, request, pk, format=None):
        person = Person.objects.get(pk=pk)
        serializer = PersonSerializer(person)
        return Response(serializer.data)

class UpdatePerson(APIView):

    @resource_checker(Person)
    def put(self, request, pk, format=None):
        person = Person.objects.get(pk=pk)
        serializer = PersonSerializer(person, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @resource_checker(Person)
    def delete(self, request, pk, format=None):
        person = Person.objects.get(pk=pk)
        person.delete()
        return Response({'msg': 'Person deleted'},status=status.HTTP_204_NO_CONTENT)