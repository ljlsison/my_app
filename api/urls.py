from django.urls import path
from api import views as api_views

urlpatterns = [
    path('persons/', api_views.ViewAllPersons.as_view(), name = "all-persons"),
    path('person/<int:pk>', api_views.PersonView.as_view(), name = "view-person"),
    path('persons/<int:pk>', api_views.UpdatePerson.as_view(), name = "update-person")
]
